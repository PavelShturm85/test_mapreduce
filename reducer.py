#!/usr/bin/env python3
#from mapper import start_mapper 
import sys 
 
 
prev_key = None 
values = 0 
def save_to_file(line):
    with open("text2.txt", "a") as w:
        w.writelines(line + "\n")
    
for line in sys.stdin: 
    key, value = line.split("\t")
    print(key, value) 
    if prev_key and key != prev_key: 
        result_key, result_value = prev_key, values
        result = f"{result_key}\t{str(result_value)}"
        print(result)
        save_to_file(result) 
        values = 0 
    prev_key = key 
    values += 1
 
if prev_key: 
    result_key, result_value = prev_key, values
    result = f"{result_key}\t{str(result_value)}"
    print(result)
    save_to_file(result) 

"""/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-streaming-1.0.3.jar -mapper "python3 mapper.py" -reducer "python3 reducer.py" -input /home/master/input/Vojna_i_mir._Tom.txt -output output"""
