#!/usr/bin/env python
 
import sys
import re
from collections import Counter

def read_input(text_file): 
    # читаем из стандартного входа
    for line in text_file: # для каждой поступающей строки
        # удаляем пробелы в начале и конце строки
        yield line.strip()


def main(separator='\t'):
    data = read_input(sys.stdin)
    for words in data:
        for word in words.split():
            word = re.sub(r'[^a-zа-я]', '', word.lower()) # оставляем только буквы
            print(f'{word}{separator}{1}')
            
if __name__ == "__main__":
    main()   

# echo "foo foo quux labs foo bar quux" | /home/pavel-shturm/test/mapreduce/mapper.py | sort -k1,1 | /home/pavel-shturm/test/mapreduce/reducer.py